package Calculate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class index {

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            //Проверка на ввод строки
            try{
                String input = br.readLine();
                Pattern pNum = Pattern.compile("^\\-\\d|^\\-|\\((\\-\\d)\\)|\\((\\-\\d)|[0-9]*[.,]?[0-9]|[+\\-*/()]");
                List<String> acts = new ArrayList<>();
                List<String> Substring;
                int leftBracket = 0;
                int rightBracket = 0;
                for (int i =0; i<input.length(); i++){
                    if(input.charAt(i)=='('){
                        leftBracket++;
                    }else if(input.charAt(i)==')'){
                        rightBracket++;
                    }
                }
                if(leftBracket!=rightBracket){
                    throw new Exception("Пропущена скобка");
                }
                if(!input.matches("[\\d+\\-*/(),.]+")){
                    throw new Exception("Присутсвуют неверные символы или строка пуста");
                }

            Matcher mNum = pNum.matcher(input);

            while (mNum.find()){
                if(mNum.group(1) != null){
                    acts.add(mNum.group(1));
                }else if(mNum.group(2) != null){
                    acts.add("(");
                    acts.add(mNum.group(2));
                }else{
                    acts.add(mNum.group());
                }
            }

            Integer start=null;
            Integer end=null;

            for (int i = 0; i < acts.size(); i++) {
                if(acts.get(i).equals("(")){
                     start = i;
                }
                if(acts.get(i).equals(")")){
                     end = i+1;
                }
                if(start!=null && end!=null) {
                    Substring = acts.subList(start, end);
                    Substring = Arithmetic(Substring,"*","/");
                    Substring = Arithmetic(Substring,"+","-");
                    String substringResult = Substring.get(1);
                    Substring.clear();
                    acts.add(start,substringResult);
                    if(acts.get(start-1).equals("-")){
                       acts.set(start-1,"-1");
                       acts.add(start,"*");

                    }
                    i = 0;
                    start = null;
                    end = null;
                }
            }
            acts = Arithmetic(acts,"*","/");
            acts = Arithmetic(acts,"+","-");

            if(acts.size()!=1){
                throw new Exception("Пропущен знак");
            }
            for(String sum:acts){
                System.out.println(sum);
            }
            }catch (Exception ex){
                System.out.println(ex.getMessage());
                System.exit(0);
            }
        }
    }

    //Тупо вычисления
    private static Double action( Double leftNumber, Double rightNumber, String action){
        Double result = 0.0;
        switch (action){
            case ("+"):
                result = leftNumber+rightNumber;
                break;
            case ("-"):
                result = leftNumber-rightNumber;
                break;
            case ("*"):
                result = leftNumber*rightNumber;
                break;
            case ("/"):
                result = leftNumber/rightNumber;
                break;
        }
        return result;
    }
//Рачет в скобках
    private static List Arithmetic(List<String> list, String signOne, String signTwo){
        try {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).equals(signOne) || list.get(j).equals(signTwo)) {
                    String leftNumber = list.get(j - 1);
                    String rightNumber = list.get(j + 1);
                    String action = list.get(j);
                    Double result = action(Double.parseDouble(leftNumber), Double.parseDouble(rightNumber), action);
                    for (int n = 0; n < 3; n++) {
                        list.remove(j - 1);
                    }
                    if (list.isEmpty()) {
                        list.add(result.toString());
                    } else {
                        list.add(j - 1, result.toString());
                    }
                    j = j - 2;
                }
            }
        }catch (IndexOutOfBoundsException ex){
            System.out.println("Ошбика при вычислении. Пропущен знак или значение");
            System.exit(0);
        }catch (NumberFormatException ex){
            System.out.println("Ошбика при вычислении. Присутсвует лишний знак");
            System.exit(0);
        }
        return list;
    }
}
